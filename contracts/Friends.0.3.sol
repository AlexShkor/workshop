pragma solidity ^0.4.6;

contract PandaFriends {
  
  Member[] members;

  struct Member {
    address member;
    bytes32 name;
    address invitedBy;
  }

  function PandaFriends(bytes32 name) {
    addMember(msg.sender, name, this);
  }

  function invite(address target, bytes32 name) {
    addMember(target, name, msg.sender);
  }

 function addMember(address target, bytes32 name, address invitedBy ) private {
    members.push(Member(target, name, invitedBy));
 }
}