pragma solidity ^0.4.6;

contract PandaFriends {
  
  event Transfer(address indexed _from, address indexed _to, uint256 _value);

  Member[] members;

  uint public totalSuply;

  
  mapping(address => uint) public balances;

  mapping(address => bool) public isMember;

  mapping(address => uint) public ids;

  struct Member {
    address member;
    string name;
    address invitedBy;
  }

  function PandaFriends(string name, uint _totalSuply) {
    totalSuply = _totalSuply;
    balances[this] = totalSuply/2;
    balances[msg.sender] = totalSuply/2;
    addMember(msg.sender, name, this);
  }

  function invite(address target, string name, uint bonus) onlyMember {
    if (balances[this] < bonus) revert();
    addMember(target, name, msg.sender);
    balances[this] -= bonus;
    balances[target] += bonus;
    if (!sendCoin(target, bonus)) {
        throw;
    }
  }

 function addMember(address target, string name, address invitedBy) private {
    if (isMember[target]) revert();
    uint id = members.length;
    members.push(Member(target, name, invitedBy));
    isMember[target] = true;
    ids[target] = id;
 }

 function getMemberName(address target) constant returns(string name) {
    if (!isMember[target]) revert();
    return members[ids[target]].name;
 }

function sendCoin(address receiver, uint amount) returns(bool sufficient) {
    if (balances[msg.sender] < amount) return false;
    if (!isMember[msg.sender] && amount > 100) revert();
    balances[msg.sender] -= amount;
    balances[receiver] += amount;
    Transfer(msg.sender, receiver, amount);
    return true;
}


uint public proposedAmount;

address[] public voters;

uint public quorum = 60;

function proposeEmission(uint amount) onlyMember {
  if (proposedAmount > 0 ) revert();
  proposedAmount = amount;
  voters.push(msg.sender);
}

function voteForEmission() onlyMember {
  if (hasVoted(msg.sender)) revert();
  voters.push(msg.sender);
  if (isQuorum()) {
    balances[this] += proposedAmount;
    voters.length = 0;
    proposedAmount = 0;
  }
}

function isQuorum() constant returns(bool) {
  return votedPercent() >= quorum;
} 

function votedPercent() constant returns(uint) {
  return voters.length*100 / members.length;
}

function hasVoted(address voter) constant returns(bool) {
  for (var index = 0; index < voters.length; index++) {
    if (voters[index] == voter) {
      return true;
    }
  }
  return false;
}

  modifier onlyMember{
      if (isMember[msg.sender]) {
        _;
      }
  }
  
}