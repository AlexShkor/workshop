pragma solidity ^0.4.6;

contract PandaFriends {
  
  Member[] members;

  mapping(address => bool) isMember;

  mapping(address => uint) ids;

  struct Member{
    address member;
    bytes32 name;
    address invitedBy;
  }

  function PandaFriends(bytes32 name) {
    addMember(msg.sender, name, this);
  }

  function invite(address target, bytes32 name) {
    addMember(target, name, msg.sender);
  }

  function addMember(address target, bytes32 name, address invitedBy ) private {
    if (isMember[target]) throw;
    uint id = members.length;
    members.push(Member(target, name, invitedBy));
    ids[target] = id;
  }
  
  function getMemberName(address target) constant returns(bytes32 name){
    return members[ids[target]].name;
  }
}