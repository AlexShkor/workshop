pragma solidity ^0.4.6;

contract PandaFriends {
  
  Member[] members;

  mapping(address => bool) isMember;

  mapping(address => uint) ids;

  struct Member {
    address member;
    string name;
    address invitedBy;
  }

  function PandaFriends(string name) {
    addMember(msg.sender, name, this);
  }

  function invite(address target, string name) onlyMember {
    addMember(target, name, msg.sender);
  }

 function addMember(address target, string name, address invitedBy ) private {
    if (isMember[target]) revert();
    uint id = members.length;
    members.push(Member(target, name, invitedBy));
    isMember[target] = true;
    ids[target] = id;
 }

 function getMemberName(address target) constant returns(string name) {
   return members[ids[target]].name;
 }
 
  modifier onlyMember{
      if (isMember[msg.sender]) {
        _;
      }
  }
}