// Импортируем библиотеку web3
Web3 = require('web3')

fs = require('fs');
//создаеи инстанс web3 сразу привязывая его к наше локально ноде/эмулятору
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))

// считываем исходный код контракта
code = fs.readFileSync("contracts/Friends.4.final.sol").toString()

//компилируем контракт
contract = web3.eth.compile.solidity(code)

//создаем интерфейс контракта
FriendsContract = web3.eth.contract(contract.info.abiDefinition)

//деплоем контракт указывая параметры конструктора
deployedContract = FriendsContract.new('Alex Shkor', 1000, {data: contract.code, from: web3.eth.accounts[0], gas: 4700000})

//привязываем интерфейс контракта к адресу в сети эфира
contractInstance = FriendsContract.at(deployedContract.address)






















// алиасы аккаунтов
acc = web3.eth.accounts[0];
acc1 = web3.eth.accounts[1]
acc2 = web3.eth.accounts[2]

//проверяем является ли создатель контракта мембером
contractInstance.isMember(acc)

//проверяем баланс
contractInstance.balances(acc)

//добавляем нового мембера
contractInstance.invite(acc1, "First Last", 100, {from: acc, gas: 400000})

contractInstance.isMember(acc1)
contractInstance.balances(acc1)

//отправляем немного коинов
contractInstance.sendCoin(acc1, 200, {from: acc, gas: 400000})