/*
pragma solidity ^0.4.6;
// We have to specify what version of compiler this code will compile with

contract PandaFriends {
  
  private Member[] members;
  
  mapping(address => bool) isMember;

  private Proposal[] nominates;

  mapping(address => bool) moderators;

  uint8 constant qvorum = 70;

  struct Member{
    address member;
    bytes32 name;
    address invitedBy;
  }

  struct Proposal{
    address candidate;
    uint totalVotes;
    bool toExclude;
    uint started;
    uint expiration;
    mapping (address => bool) votes;
  }

  mapping (address => Proposal) proposals;

  address[] moderators;

  function PandaFriends(bytes32 name) {
    addMember(msg.sender, name);
    moderators.push(msg.sender);
  }

  function join(bytes32 name) returns (uint8) {
    //if (msg.value == 0) throw;
    addMember(msg.sender, name);
  }

  function nominateToJoin(address candidate) onlyModerator {
    proposals[candidate] = Proposal({
      candidate: candidate,
      totalVotes: 1,
      toExclude: false,
      started: now,
      expiration: 120 hours
    });
    Proposal proposal = proposals[candidate];
    proposal.votes[msg.sender] = true;
  }

  function addMember(address member, bytes32 name) private {
      members[member] = Member({
        member: member,
        name: name
      });
  }

  function vote(address candidate) onlyModerator {
     var proposal = proposals[candidate];
     if (proposal.totalVotes == 0) throw;
     proposal.votes[msg.sender] = true;
  }

  function finishProposal(){

  }

  modifier onlyModerator {
    for (var index = 0; index < moderators.length; index++) {
      if (moderators[index] == msg.sender)
      {
        _;
      }
    }
  }
}